import React, { Component } from 'react';
import { Fragment } from 'react';
import PropTypes from 'prop-types';

class ContentTemplate extends Component {
    static propTypes = {
        children: PropTypes.element.isRequired
    }

    render() {
        return (
            <Fragment>
                <header>

                </header>
                <main className="main">
                    <div className="container">
                        {this.props.children}
                    </div>
                </main>
                <footer className='footer'>

                </footer>
            </Fragment>
        )
    }
}

export default ContentTemplate;