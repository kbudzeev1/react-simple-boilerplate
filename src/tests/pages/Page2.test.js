import React from 'react';
import { shallow } from 'enzyme';
import Page2 from '../../components/pages/Page2';

describe('Page1', () => {
  it('should render', () => {
    const wrapper = shallow(<Page2 />);
    expect(wrapper.find('.page-2')).toHaveLength(1);
  });
});
