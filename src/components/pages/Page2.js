import React, { Component } from 'react';

class Page2 extends Component {
  render() {
    return (
      <div className='page page-2'>Page 2</div>
    );
  }
}

export default Page2;
