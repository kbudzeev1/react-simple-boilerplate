import React from 'react';
import { shallow } from 'enzyme';
import Icon from '../../../components/icon';

describe('Icon', () => {
  it('should render with default width = 10', () => {
    const wrapper = shallow(<Icon></Icon>);
    expect(wrapper.find('svg').props().width).toEqual(10);
  });
})