import React from 'react';
import { shallow } from 'enzyme';
import Page1 from '../../components/pages/Page1';

describe('Page1', () => {
  it('should render', () => {
    const wrapper = shallow(<Page1 />);
    expect(wrapper.find('.page-1')).toHaveLength(1);
  });
});
