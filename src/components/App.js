import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './store/history';

import Page1 from './pages/Page1';
import Page2 from './pages/Page2';
import Page3 from './pages/Page3';
import PageTemplate from './templates/PageTemplate';

export default class App extends Component {
  render() {
    return (
      <Router history={history}>
        <PageTemplate>
          <Switch>
            <Route path="/" exact component={() => <Page1 />} />
            <Route path="/page2" exact component={() => <Page2 />} />
            <Route path="/page3" exact component={() => <Page3 />} />
          </Switch>
        </PageTemplate>
      </Router>
    );
  }
}
