import React from 'react';
import { shallow } from 'enzyme';
import Page3 from '../../components/pages/Page3';

describe('Page3', () => {
  it('should render', () => {
    const wrapper = shallow(<Page3 />);
    expect(wrapper.find('.page-3')).toHaveLength(1);
  });
});
