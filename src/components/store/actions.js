import {
  _API_TEST_URL_
} from './utils/apiURL';

export const SOME_ASYNC_ACTION = 'SOME_ASYNC_ACTION';
export const SOME_ASYNC_ACTION_SUCCESS = 'SOME_ASYNC_ACTION_SUCCESS';
export const SOME_ASYNC_ACTION_FAILURE = 'SOME_ASYNC_ACTION_FAILURE';

export const someAsyncAction = () => ({
  type: SOME_ASYNC_ACTION
});

export const someAsyncActionSuccess = () => ({
  type: SOME_ASYNC_ACTION_SUCCESS
});

export const someAsyncActionFailure = () => ({
  type: SOME_ASYNC_ACTION_FAILURE
});
